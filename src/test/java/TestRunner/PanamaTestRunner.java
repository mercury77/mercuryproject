package TestRunner;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features ="Feature",
glue = {"Stepdef","BaseStepDef"},
monochrome =true,
tags = {"@Sanity"},
plugin = {"pretty", "html:target/cucumber-jvm-reports",
		"json:target/cucumber-json/cucumber.json",
		"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-Extent-reports/report.html"
}
		)

public class PanamaTestRunner 
{
	@AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig("src/test/resources/extent-config.xml");
    }

}

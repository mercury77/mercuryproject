package Stepdef;

import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CancelTransactionUsingReference 
{
	WebDriver driver=Parent.getDriver();
	FileInputStream file;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row1;
	String url;
	String username;
	String password;
	int Ref;
	String Refno;
	JavascriptExecutor js=(JavascriptExecutor)driver;


	@Then("^User call Excel file$")
	public void user_call_Excel_file() throws Throwable 
	{   
		file=new FileInputStream("C:\\Panama.xlsx");
		workbook=new XSSFWorkbook(file);
		sheet=workbook.getSheet("sheet1");
		row1=sheet.getRow(1);

		username=row1.getCell(0).toString();
		password=row1.getCell(1).toString();
		url=row1.getCell(2).toString();
		Ref=(int) row1.getCell(3).getNumericCellValue();
		Refno=String.valueOf(Ref);
	}

	@When("^User Open URL$")
	public void user_Open_URL() throws Throwable
	{
		driver.get(url);
		driver.manage().window().maximize();
		Thread.sleep(5000);
	}

	@Then("^User Enter valid Username and valid Password$")
	public void user_Enter_valid_Username_and_valid_Password() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id=\"contentForm:signIn\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).sendKeys(password);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(10000);
	}

	@Then("^User Answer a security question$")
	public void user_Answer_a_security_question() throws Throwable 
	{
		String answer=driver.findElement(By.xpath("//*[@id=\"contentForm\"]/div[2]/div[1]/div[1]/div/span")).getText();
		answer=answer.trim();
		String words[] =answer.split(" ");
		answer=words[words.length-1];
		answer=answer.substring(0, answer.length()-1);
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).sendKeys(answer);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);    
	}

	@Then("^User Scroll Down Page$")
	public void user_Scroll_Down_Page() throws Throwable
	{
		js.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(5000);
	}

	@Then("^User Clicks on Events icon$")
	public void user_Clicks_on_Events_icon() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"paytrans_tab_Form:listViewButton\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^User Cancel Transaction using Reference no\\.$")
	public void user_Cancel_Transaction_using_Reference_no() throws Throwable 
	{
				
		int rowcount=driver.findElements(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr")).size();
		System.out.println(rowcount);

		for (int a=1;a<=rowcount;a++)
		{
			
			driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+a+"]/td[2]")).click();
			Thread.sleep(5000);
			String Row2=driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+(a+1)+"]/td[2]/div/div[1]")).getText();
			System.out.println(Row2);
			Thread.sleep(5000);
			if(Row2.contains(Refno))
			{
				driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+(a+1)+"]/td[2]/div/div[2]/a")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_CANCELCONFIRM_BUTTON\"]")).click();
				break;
			}
			driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+a+"]/td[2]")).click();
			Thread.sleep(5000);
		}
	}
	
	@Then("^User Cancel all future Transactions$")

	public void user_Cancel_all_future_Transactions() throws Throwable
	{
		driver.findElement(By.xpath("//*[@id=\"paytrans_tab_Form:moreupc-button\"]")).click();
		Thread.sleep(5000);
		int rowcount=driver.findElements(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr")).size();
		System.out.println(rowcount);
		if(rowcount>0)
		{
			for(int a=1;a<=rowcount;a++)
			{
				driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr[1]/td[2]")).click();
				Thread.sleep(5000);
				driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr[2]/td[2]/div/div[2]/a")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_CANCELCONFIRM_BUTTON\"]")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id=\"accountOverview\"]/a")).click();
				Thread.sleep(5000);
				js.executeScript("window.scrollBy(0,1000)");
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id=\"paytrans_tab_Form:listViewButton\"]")).click();
				Thread.sleep(5000);
			}
		}
	}

	
	



}

package Stepdef;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class TransferAndCancel2 
{
	WebDriver driver=Parent.getDriver();
	FileInputStream file;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row1;
	String Refno;


	@Then("^Scroll Down Page$")
	public void scroll_Down_Page() throws Throwable 
	{
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(5000);
	}
	

	@Then("^Clicks on Events icon$")
	public void clicks_on_Events_icon() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"paytrans_tab_Form:listViewButton\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^User Cancel Transaction using Reference no\\. saved in excel file$")
	public void user_Cancel_Transaction_using_Reference_no_saved_in_excel_file() throws Throwable 
	{
		file=new FileInputStream("C:\\Users\\pc\\Documents\\Panama2.xlsx");
		workbook=new XSSFWorkbook(file);
		sheet=workbook.getSheet("sheet1");
		row1=sheet.getRow(0);

		Refno= row1.getCell(0).toString();
		Thread.sleep(10000);

		try 
		{
			while(true)
			{					
				driver.findElement(By.xpath("//*[@id=\"paytrans_tab_Form:moreupc-button\"]")).click();
				Thread.sleep(5000);
			}
			
		}
		catch(NoSuchElementException e)
		{
			
		}

		int rowcount=driver.findElements(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr")).size();
		System.out.println(rowcount);

		for (int a=1;a<=rowcount;a++)
		{
			driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+a+"]/td[2]")).click();
			Thread.sleep(5000);
			String Row2=driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+(a+1)+"]/td[2]/div/div[1]")).getText();
			System.out.println(Row2);
			Thread.sleep(5000);
			if(Row2.contains(Refno))
			{
				driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+(a+1)+"]/td[2]/div/div[2]/a")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_CANCELCONFIRM_BUTTON\"]")).click();
				System.out.println("Transaction cancelled successfully");
				break;
			}
			driver.findElement(By.xpath("/html/body/div[1]/section/div[4]/div[14]/form/div[2]/div[2]/span[1]/table/tbody/tr["+a+"]/td[2]")).click();
			Thread.sleep(5000);
		}

	}



}

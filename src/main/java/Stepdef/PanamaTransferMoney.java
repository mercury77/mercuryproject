package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class PanamaTransferMoney 
{
	WebDriver driver=Parent.getDriver();

	@Then("^User click on Transfer Between Accounts$")
	public void user_click_on_Transfer_Between_Accounts() throws Throwable
	{
		driver.findElement(By.xpath("/html/body/div[1]/section/div[3]/form/a[2]")).click();
		Thread.sleep(2000);
	}

	@Then("^User Select From Account$")
	public void user_Select_From_Account() throws Throwable 
	{
		Select option=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:from_field\"]")));
		option.selectByIndex(1);
		Thread.sleep(2000);
	}

	@Then("^User Select To Account$")
	public void user_Select_To_Account() throws Throwable 
	{
		Select option2=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:to_field\"]")));
		option2.selectByIndex(1);
		Thread.sleep(2000);
	}

	@Then("^User Select amount$")
	public void user_Select_amount() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:amount1\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:amount1\"]")).sendKeys("2");
	}

	@Then("^User Select Frequency$")
	public void user_Select_Frequency() throws Throwable 
	{
		Select option3=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:frequency_field\"]")));
		option3.selectByIndex(0);
	}

	@Then("^User Select Date$")
	public void user_Select_Date() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:when_Date\"]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/table/thead/tr[1]/th[2]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/table/tbody/tr/td/span[2]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/table/tbody/tr[1]/td[6]")).click();

	}

	@Then("^User Click on Next button$")
	public void user_Click_on_Next_button() throws Throwable
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_CONFIRMTRANSFER_LINK\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^User Click on Confirm button$")
	public void user_Click_on_Confirm_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_DOCONFIRMTRANSFER_LINK\"]")).click();
		Thread.sleep(10000);
	}

	@Then("^User Confirm the Success mesg$")
	public void user_Confirm_the_Success_mesg() throws Throwable {
		String mesg=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		System.out.println(mesg);
		if(mesg.contains("Success"))
		{
			System.out.println("Successfully transfered");
		}		
	}


}

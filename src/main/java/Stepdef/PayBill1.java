package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class PayBill1 
{
	WebDriver driver=Parent.getDriver();

	@Then("^Click on Add Payee button$")
	public void click_on_Add_Payee_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:goaddPayee\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Type Payee and click on search button$")
	public void type_Payee_and_click_on_search_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:newPayeeName\"]")).sendKeys("IDAAN");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"searchid\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Choose Payee and Click on ok button$")
	public void choose_Payee_and_Click_on_ok_button() throws Throwable 
	{
		int rowcount=driver.findElements(By.xpath("/html/body/div[8]/div/div/div[2]/div/form/div/div/div[6]/table/tbody/tr")).size();

		for(int i=1;i<=rowcount;i++)
		{
			String options=driver.findElement(By.xpath("/html/body/div[8]/div/div/div[2]/div/form/div/div/div[6]/table/tbody/tr["+i+"]")).getText();
			if(options.contains("IDAAN"))
			{
				driver.findElement(By.xpath("/html/body/div[8]/div/div/div[2]/div/form/div/div/div[6]/table/tbody/tr["+i+"]")).click();
				break;
			}
		}
		driver.findElement(By.xpath("/html/body/div[8]/div/div/div[3]/button[2]")).click();
		Thread.sleep(2000);
	}

	@Then("^Click next$")
	public void click_next() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:next-button\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Type Account no$")
	public void type_Account_no() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:newPayeeNumber\"]")).sendKeys("12567890");
	}

	@Then("^Click for Next$")
	public void click_for_Next() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:next-button\"]")).click();
	}
}

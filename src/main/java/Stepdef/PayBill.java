package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class PayBill 
{
	WebDriver driver=Parent.getDriver();

	@Then("^Click on Pay a Bill$")
	public void click_on_Pay_a_Bill() throws Throwable 
	{
		driver.findElement(By.className("pay-bill")).click();
		Thread.sleep(5000);
	}

	@Then("^Select Payee$")
	public void select_Payee() throws Throwable 
	{
		int rowcount=driver.findElements(By.xpath("/html/body/div[1]/section/div[2]/form/div[2]/div[2]/div/div[3]/table/tbody/tr")).size();
		System.out.println(rowcount);
		for(int a=1;a<=rowcount;a++)
		{
			String Payee=driver.findElement(By.xpath("/html/body/div[1]/section/div[2]/form/div[2]/div[2]/div/div[3]/table/tbody/tr["+a+"]")).getText();
			if(Payee.contains("IDAAN"))
			{
				driver.findElement(By.xpath("/html/body/div[1]/section/div[2]/form/div[2]/div[2]/div/div[3]/table/tbody/tr["+a+"]")).click();
				break;
			}
		}
	}

	@Then("^Next button click$")
	public void next_button_click() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:next-button\"]")).click();
		Thread.sleep(5000);	   
	}

	@Then("^From account select$")
	public void from_account_select() throws Throwable 
	{
		Select option=new Select(driver.findElement(By.xpath("//*[@id=\"contentForm:accList\"]")));
		option.selectByIndex(1);
		Thread.sleep(2000);
	}

	@Then("^To Amount select$")
	public void to_Amount_select() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:amount\"]")).sendKeys("5");
	}

	@Then("^Frequency select$")
	public void frequency_select() throws Throwable 
	{
		Select option1=new Select(driver.findElement(By.xpath("//*[@id=\"contentForm:frequency\"]")));
		option1.selectByIndex(0);
	}

	@Then("^Date select$")
	public void date_select() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:enter_date\"]")).click();
		driver.findElement(By.xpath("/html/body/div[6]/div[1]/table/tbody/tr[2]/td[4]")).click();
	}

	@Then("^next button$")
	public void next_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:next-button\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Pay button$")
	public void pay_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:pay-button\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Get Success mesg$")
	public void get_Success_mesg() throws Throwable 
	{
		String mesg=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		if(mesg.contains("Success"))
		{
			System.out.println("Bill Payment successfull");
			Thread.sleep(5000);
		}
	}



}

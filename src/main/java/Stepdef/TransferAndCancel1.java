package Stepdef;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import BaseStepDef.Parent;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransferAndCancel1 
{
	WebDriver driver=Parent.getDriver();
	FileInputStream file;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row1;
	String url;
	String username;
	String password;
	int Ref;
	String Refno;

	@Then("^Call Panama Excel file$")
	public void call_Panama_Excel_file() throws Throwable 
	{
		file=new FileInputStream(System.getProperty("user.dir")+"/ExcelFiles/Panama2.xlsx");
		workbook=new XSSFWorkbook(file);
		sheet=workbook.getSheet("sheet1");
		row1=sheet.getRow(1);

		username=row1.getCell(0).toString();
		password=row1.getCell(1).toString();
		url=row1.getCell(2).toString();
	}


	@When("^Open URL$")
	public void open_URL() throws Throwable 
	{
		driver.get(url);
		driver.manage().window().maximize();
		Thread.sleep(5000);
	}

	@Then("^Enter valid Username and valid Password$")
	public void enter_valid_Username_and_valid_Password() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id=\"contentForm:signIn\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).sendKeys(password);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(10000);
	}

	@Then("^Answer security question$")
	public void answer_security_question() throws Throwable 
	{
		String answer=driver.findElement(By.xpath("//*[@id=\"contentForm\"]/div[2]/div[1]/div[1]/div/span")).getText();
		answer=answer.trim();
		String words[] =answer.split(" ");
		answer=words[words.length-1];
		answer=answer.substring(0, answer.length()-1);
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).sendKeys(answer);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);    
	}

	@Then("^click on Transfer Between Accounts$")
	public void click_on_Transfer_Between_Accounts() throws Throwable 
	{
		driver.findElement(By.xpath("/html/body/div[1]/section/div[3]/form/a[2]")).click();
		Thread.sleep(2000);
	}

	@Then("^Select From Account$")
	public void select_From_Account() throws Throwable 
	{
		Select option=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:from_field\"]")));
		option.selectByIndex(1);
		Thread.sleep(2000);
	}

	@Then("^Select To Account$")
	public void select_To_Account() throws Throwable 
	{
		Select option2=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:to_field\"]")));
		option2.selectByIndex(1);
		Thread.sleep(2000);
	}

	@Then("^Select amount$")
	public void select_amount() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:amount1\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:amount1\"]")).sendKeys("2");
	}

	@Then("^Select Frequency$")
	public void select_Frequency() throws Throwable 
	{
		Select option3=new Select(driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:frequency_field\"]")));
		option3.selectByIndex(0);
	}

	@Then("^Select Date$")
	public void select_Date() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:when_Date\"]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/table/thead/tr[1]/th[2]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/table/tbody/tr/td/span[2]")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/table/tbody/tr[1]/td[6]")).click();
	}

	@Then("^Click on Next button$")
	public void click_on_Next_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_CONFIRMTRANSFER_LINK\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Click on Confirm button$")
	public void click_on_Confirm_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"fundtransDetailsForm:FT_DOCONFIRMTRANSFER_LINK\"]")).click();
		Thread.sleep(10000);
	}

	@Then("^Confirm the Success mesg and Write in Excel file$")
	public void confirm_the_Success_mesg_and_Write_in_Excel_file() throws Throwable 
	{
		String mesg=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		System.out.println(mesg);

		//     To get particular text which contains ref no.   
		mesg=mesg.substring(mesg.indexOf("reference"), mesg.length());
		System.out.println(mesg);

		//	   To extract only numeric values from the string value 
		mesg=mesg.replaceAll("[^0-9]", "");
		System.out.println(mesg);

		//		To write extract ref no in excel file
		FileOutputStream filee=new FileOutputStream("C:\\Users\\pc\\Documents\\Panama2.xlsx");
		XSSFWorkbook workbookk=new XSSFWorkbook();
		XSSFSheet sheett=workbookk.createSheet("sheet1");
		XSSFRow roww=sheett.createRow(0);
		Cell cell=roww.createCell(0);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(mesg);
		workbookk.write(filee);
		filee.close();
		System.out.println("Excel writing ended");
	}

	@Then("^Logout and Exit Browser$")
	public void logout_and_Exit_Browser() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"menuForm:signOutsubmitbtn\"]")).click();
		Thread.sleep(10000);
	}



}

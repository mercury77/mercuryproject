package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class PasswordChange 
{
	WebDriver driver=Parent.getDriver();
	
	

	@Then("^User click on Settings icon$")
	public void user_click_on_Settings_icon() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"nav-settings\"]/a")).click();
		Thread.sleep(5000);
	}

	@Then("^User click on Security settings$")
	public void user_click_on_Security_settings() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"menuItems_settings\"]/li[4]/a")).click();
		Thread.sleep(5000);
	}

	@Then("^User enter old password as \"([^\"]*)\"$")
	public void user_enter_old_password_as(String arg1) throws Throwable 
{
		driver.findElement(By.xpath("//*[@id=\"changPasswdForm:oldPwdMasked\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"changPasswdForm:oldPwdMasked\"]")).sendKeys(arg1);
	}

	@Then("^User enter new password as \"([^\"]*)\"$")
	public void user_enter_new_password_as(String arg1) throws Throwable
{
		driver.findElement(By.xpath("//*[@id=\"changPasswdForm:newPwdMasked\"]")).sendKeys(arg1);
		Thread.sleep(2000);
	}

	@Then("^User confirms new password \"([^\"]*)\"$")
	public void user_confirms_new_password(String arg1) throws Throwable 
{
		driver.findElement(By.xpath("//*[@id=\"changPasswdForm:retypePwdMasked\"]")).sendKeys(arg1);
		Thread.sleep(2000);
	}

	@Then("^User click on confirm button$")
	public void user_click_on_confirm_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"changPasswdForm:btnchangePasswdId\"]")).click();
		Thread.sleep(10000);
	}
	
	@Then("^User Click on Change security phrase button$")
	public void user_Click_on_Change_security_phrase_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"manageSecuritySettingsForm:presonalPhraseid\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^User write new security phrase$")
	public void user_write_new_security_phrase() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"changPerPhaseForm:newPersonalPhrase\"]")).sendKeys("Testing is good");
		Thread.sleep(5000);
	}

	@Then("^User click on submit button$")
	public void user_click_on_submit_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"changPerPhaseForm:btnChangPerPhaseSbId\"]")).click();
		Thread.sleep(5000);
	}
	





}

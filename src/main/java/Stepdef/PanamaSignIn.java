package Stepdef;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PanamaSignIn
{
	WebDriver driver=Parent.getDriver();
	
	@When("^User Open URL as \"([^\"]*)\"$")
	public void user_Open_URL_as(String arg1) 
	{
		driver.get(arg1);
		driver.manage().window().maximize();
	}

	@Then("^User Enter valid Username as \"([^\"]*)\" and valid Password as \"([^\"]*)\"$")
	public void user_Enter_valid_Username_as_and_valid_Password_as(String arg1, String arg2) throws Throwable 
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).sendKeys(arg1);
		driver.findElement(By.xpath("//*[@id=\"contentForm:signIn\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).sendKeys(arg2);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);
	}

	@Then("^Answer a security question$")
	public void answer_a_security_question() throws Throwable 
	{
		String answer=driver.findElement(By.xpath("//*[@id=\"contentForm\"]/div[2]/div[1]/div[1]/div/span")).getText();
		answer=answer.trim();
		String words[] =answer.split(" ");
		answer=words[words.length-1];
		answer=answer.substring(0, answer.length()-1);
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).sendKeys(answer);
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);    
	}

	@Then("^User Logout$")
	public void user_Logout() throws Throwable
	{
		Assert.assertEquals(driver.getTitle(), "Bachan");
		driver.findElement(By.xpath("//*[@id=\"menuForm:signOutsubmitbtn\"]")).click();
		Thread.sleep(10000);
	}
}


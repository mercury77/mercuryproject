package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FailedTest 
{
	WebDriver driver=Parent.getDriver();

	@When("^Open URL \"([^\"]*)\"$")
	public void open_URL(String arg1) throws Throwable 
	{
		driver.get(arg1);
		driver.manage().window().maximize();
		Thread.sleep(5000);
	}

	@When("^Enter wrong username as \"([^\"]*)\"$")
	public void enter_wrong_username_as(String arg1) throws Throwable
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).sendKeys(arg1);	   
	}

	@Then("^Click on signin button$")
	public void click_on_signin_button() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:signIn\"]")).click();
		Thread.sleep(5000);	   
	}

	@Then("^Get the error mesg of username$")
	public void get_the_error_mesg_of_username() throws Throwable 
	{
		String ErrorMesg1=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		if(ErrorMesg1.contains("Error"))
		{
			System.out.println("Error mesg of username is verified successfully");
		}
	}

	@Then("^Enter Valid username \"([^\"]*)\"$")
	public void enter_Valid_username(String arg1) throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"contentForm:nscard\"]")).sendKeys(arg1);	   
	}

	@Then("^Enter wrong password as \"([^\"]*)\"$")
	public void enter_wrong_password_as(String arg1) throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).sendKeys(arg1);
	}

	@Then("^Click next for next page$")
	public void click_next_for_next_page() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);    
	}

	@Then("^Get the error mesg of password$")
	public void get_the_error_mesg_of_password() throws Throwable 
	{
		String errorMesg2=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		if(errorMesg2.contains("Error"))
		{
			System.out.println("Error mesg of password is verified successfully");
		}  
	}

	@Then("^Enter Valid password as \"([^\"]*)\"$")
	public void enter_Valid_password_as(String arg1) throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"contentForm:pwdMasked\"]")).sendKeys(arg1);
	}

	@Then("^Answer security question wrong$")
	public void answer_security_question_wrong() throws Throwable
	{
		String answer=driver.findElement(By.xpath("//*[@id=\"contentForm\"]/div[2]/div[1]/div[1]/div/span")).getText();
		answer=answer.trim();
		String words[] =answer.split(" ");
		answer=words[words.length-1];
		answer=answer.substring(0, answer.length());
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).sendKeys(answer); 	    
	}

	@Then("^Click on signin$")
	public void click_on_signin() throws Throwable 
	{
		driver.findElement(By.xpath("//*[@id=\"contentForm:submit\"]")).click();
		Thread.sleep(5000);    	    
	}

	@Then("^Get error mesg of security question$")
	public void get_error_mesg_of_security_question() throws Throwable 
	{
		String errorMesg3=driver.findElement(By.xpath("//*[@id=\"messages\"]/div/span[2]")).getText();
		if(errorMesg3.contains("Error"))
		{
			System.out.println("Error mesg of security question is verified successfully");
		}
	}

	@Then("^Answer security question right$")
	public void answer_security_question_right() throws Throwable 
	{
		String answer=driver.findElement(By.xpath("//*[@id=\"contentForm\"]/div[2]/div[1]/div[1]/div/span")).getText();
		answer=answer.trim();
		String words[] =answer.split(" ");
		answer=words[words.length-1];
		answer=answer.substring(0, answer.length()-1);
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"contentForm:questionAnsw:0:answer1Masked\"]")).sendKeys(answer);
	}

	@Then("^varify Title of the Signin Page$")
	public void varify_Title_of_the_Signin_Page() throws Throwable 
	{
		if(driver.getTitle().equals("Scotia OnLine"))
		{
			System.out.println("Successfull Signin");
		}
	}
}

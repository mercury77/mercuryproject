package Stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import BaseStepDef.Parent;
import cucumber.api.java.en.Then;

public class Masking 
{
	WebDriver driver=Parent.getDriver();
	JavascriptExecutor js=(JavascriptExecutor)driver;
	
	@Then("^User click on Account settings$")
	public void user_click_on_Account_settings() throws Throwable 
	{
	    driver.findElement(By.xpath("//*[@id=\"menuForm:accountSettingsCmdLink\"]")).click();
	    Thread.sleep(5000);
	}

	@Then("^User operate account masking$")
	public void user_operate_account_masking() throws Throwable 
	{
		WebElement Element=driver.findElement(By.xpath("//*[@id=\"acctUnmaskSelectOneRadio:0\"]"));
		js.executeScript("arguments[0].scrollIntoView()",Element );	 //To scroll window untill the element is visible 
		


		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//*[@id=\"acctUnmaskSelectOneRadio:0\"]")).isSelected())
		{
			driver.findElement(By.xpath("//*[@id=\"acctUnmaskSelectOneRadio:1\"]")).click();
		}
		
		else
		{
			driver.findElement(By.xpath("//*[@id=\"acctUnmaskSelectOneRadio:0\"]")).click();
		}
	    
	}

	@Then("^User submit$")
	public void user_submit() throws Throwable 
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"actionButtonPA\"]")).click();
		Thread.sleep(5000);
	}


}

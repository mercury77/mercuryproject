package BaseStepDef;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Parent 
{
	public static WebDriver driver;

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		Parent.driver = driver;
	}

	@Given("^Open Chrome Browser$")
	public void open_Chrome_Browser() throws Throwable 
	{
		WebDriverManager.chromedriver().setup();
		if(driver==null)
		{
			driver=new ChromeDriver();	   
		}
	}
	@After(order=0)
	public void EmbedScreenshot(Scenario scenario)
	{
		((JavascriptExecutor)driver).executeScript("document.body.style.zoom='0.7' ");
		if(scenario.isFailed())
		{
			scenario.getName();
			try 
			{
				byte[ ] screenshot=((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			}
			
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			byte[ ] screenshot=((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
          //			This code is for the use of screenshot
         //			    This is changed by Rajbeer 
		}
	}
}

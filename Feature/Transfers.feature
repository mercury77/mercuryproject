Feature: Login Transfer And Logout

  @Regression1 @Sanity 
  Scenario Outline: Money Transaction
    Given Open Chrome Browser
    When User Open URL as "<url>"
    Then User Enter valid Username as "<username>" and valid Password as "<password>"
    Then Answer a security question
    Then User click on Transfer Between Accounts
    Then User Select From Account
    Then User Select To Account
    Then User Select amount
    Then User Select Frequency
    Then User Select Date
    Then User Click on Next button
    Then User Click on Confirm button
    Then User Confirm the Success mesg
    Then User Logout

    Examples: 
      | username     | password  | url                                                                                                 |
      | Mtucklerpre1 | Password1 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |

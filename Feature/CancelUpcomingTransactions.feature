#Author: Rajbeer Kaur
Feature: Cancel Transactions

  @Regrassion3
  Scenario Outline: Cancel Upcoming Transactions
    Given Open Chrome Browser
    When User Open URL as "<url>"
    Then User Enter valid Username as "<username>" and valid Password as "<password>"
    Then User Answer a security question
    Then User Scroll Down Page
    Then User Clicks on Events icon
    Then User Cancel all future Transactions 
    Then User Logout
   

    Examples: 
      | username     | password  | url                                                                                                 |
      | Mtucklerpre1 | Password1 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |

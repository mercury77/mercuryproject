#Author: Rajbeer Kaur
Feature: Pay Bill

  #@Regression7
  #Scenario: Pay a Bill
    #Given Open Chrome Browser
    #Then Call Panama Excel file
    #When Open URL
    #Then Enter valid Username and valid Password
    #Then Answer security question
    #Then Click on Pay a Bill
    #Then Select Payee
    #Then Next button click
    #Then From account select
    #Then To Amount select
    #Then Frequency select
    #Then Date select
    #Then next button
    #Then Pay button
    #Then Get Success mesg
    #Then Logout and Exit Browser

  @Regression8
  Scenario: Pay a Bill and add new Payee
    Given Open Chrome Browser
    Then Call Panama Excel file
    When Open URL
    Then Enter valid Username and valid Password
    Then Answer security question
    Then Click on Pay a Bill
    Then Click on Add Payee button
    Then Scroll Down Page
    Then Type Payee and click on search button
    Then Choose Payee and Click on ok button
    Then Click next
    Then Type Account no
    Then Click for Next
    Then Logout and Exit Browser

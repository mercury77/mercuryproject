#Author: Rajbeer Kaur
Feature: Password Change

  @Regression
  Scenario Outline: Change Password
    Given Open Chrome Browser
    When User Open URL as "<url>"
    Then User Enter valid Username as "<username>" and valid Password as "<password>"
    Then Answer a security question
    Then User click on Settings icon 
    Then User click on Security settings
    Then User enter old password as "<password>"
    Then User enter new password as "<newpassword>"
    Then User confirms new password "<newpassword>"
    Then User click on confirm button
    Then User Logout

    Examples: 
      | username     | password  | newpassword | url                                                                                                 |
      | Mtucklerpre1 | Password1 | Password2 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |
      | Mtucklerpre1 | Password2 | Password1 |https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |
      
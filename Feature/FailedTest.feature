#Author Rajbeer Kaur
Feature: Verify Failed test result

  @Regression
  Scenario Outline: Failed test
    Given Open Chrome Browser
    When Open URL "<URL>"
    When Enter wrong username as "<WrongUsername>"
    Then Click on signin button
    Then Get the error mesg of username
    Then Enter Valid username "<ValidUsername>"
    Then Click on signin button
    Then Enter wrong password as "<WrongPassword>"
    Then Click next for next page
    Then Get the error mesg of password
    Then Enter Valid password as "<ValidPassword>"
    Then Click next for next page
    Then Answer security question wrong
    Then Click on signin
    Then Get error mesg of security question
    Then Answer security question right
    Then Click on signin
    Then varify Title of the Signin Page
    Then Logout and Exit Browser

    Examples:
| WrongUsername | ValidUsername | WrongPassword | ValidPassword |URL|
| mtucklerpre1 | Mtucklerpre1 | password1 | Password1 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |


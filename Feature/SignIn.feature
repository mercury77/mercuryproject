#Author: Rajbeer Kaur
Feature: Login And Logout

  @Regression @Sanity @test
  Scenario Outline: Sign In
    Given Open Chrome Browser
    When User Open URL as "<url>"
    Then User Enter valid Username as "<username>" and valid Password as "<password>"
    Then Answer a security question
    Then User Logout

    Examples: 
      | username     | password  | url                                                                                                 |
      | Mtucklerpre1 | Password1 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |

Feature: Masking

  @Reg
  Scenario: Account Masking
    Given Open Chrome Browser
    Then Call Panama Excel file
    When Open URL
    Then Enter valid Username and valid Password
    Then Answer security question
    Then User click on Settings icon
    Then User click on Account settings
    Then User operate account masking
    Then User submit
    Then Logout and Exit Browser

#Author: Rajbeer Kaur
Feature: Cancel Transaction using ref no.

  @Regression4
  Scenario: Cancel Transaction using Reference no.
    Given Open Chrome Browser
    Then User call Excel file
    When User Open URL
    Then User Enter valid Username and valid Password
    Then User Answer a security question
    Then User Scroll Down Page
    Then User Clicks on Events icon
    Then User Cancel Transaction using Reference no.
    Then User Logout
    

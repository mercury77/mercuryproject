#Author: Rajbeer Kaur
Feature: Transfer & Cancel

  @Regression5
  Scenario: Transfer money
    Given Open Chrome Browser
    Then Call Panama Excel file
    When Open URL
    Then Enter valid Username and valid Password
    Then Answer security question
    Then click on Transfer Between Accounts
    Then Select From Account
    Then Select To Account
    Then Select amount
    Then Select Frequency
    Then Select Date
    Then Click on Next button
    Then Click on Confirm button
    Then Confirm the Success mesg and Write in Excel file
    Then Logout and Exit Browser

  @Regression6
  Scenario: Cancel the same Transaction
    Given Open Chrome Browser
    Then Call Panama Excel file
    When Open URL
    Then Enter valid Username and valid Password
    Then Answer security question
    Then Scroll Down Page
    Then Clicks on Events icon
    Then User Cancel Transaction using Reference no. saved in excel file
    Then Logout and Exit Browser

#Author: Rajbeer Kaur
Feature: Security Phrase Change

  @Regression3
  Scenario Outline: Change Phrase
    Given Open Chrome Browser
    When User Open URL as "<url>"
    Then User Enter valid Username as "<username>" and valid Password as "<password>"
    Then Answer a security question
    Then User click on Settings icon
    Then User click on Security settings
    Then User Click on Change security phrase button
    Then User write new security phrase
    Then User click on submit button 
    Then User Logout

    Examples: 
      | username     | password  | url                                                                                                 |
      | Mtucklerpre1 | Password1 | https://www.ppleap.scotiabank.com/onlineV3/leap/signon/signOn.xhtml?country=PAN&lang=en&channel=WEB |
